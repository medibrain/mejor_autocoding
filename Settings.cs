﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml.Linq;

namespace MejorAutoCoding
{
    class Settings
    {
      
        public static System.Data.DataSet dsSettings = new System.Data.DataSet();//Settings用DataSet
        public static System.Data.DataTable dtServerSettings = new System.Data.DataTable();//サーバ設定定義用DataTable

        

        static string fileName = System.Windows.Forms.Application.StartupPath + "\\settings.xml";
        static XDocument xdoc;

        public enum mejor_enviroment
        {
            Production,
            InternalServer,
            Test,
            Local,
            Other,
        }


        static Settings()
        {
            if (System.IO.File.Exists(fileName))
            {
                xdoc = XDocument.Load(fileName);
            }
            else
            {
                xdoc = new XDocument();
                xdoc.Add(new XElement("Settings"));
                xdoc.Save(fileName);
            }
        }


        private static void setValue(string name, string value)
        {
            xdoc.Element("Settings").SetElementValue(name, value);
            xdoc.Save(fileName);
        }

        private static string getValue(string name)
        {
            if (xdoc.Element("Settings").Element(name) == null) return string.Empty;
            return xdoc.Element("Settings").Element(name).Value;
        }


       /// <summary>
       /// サーバ設定をDataSetにロード
       /// </summary>
        public static void getServerSettings()
        {            
            dsSettings.ReadXml("settings.xml");
            dtServerSettings = dsSettings.Tables["AWS"];
                        
        }


        /// <summary>
        /// 共通設定プリンタ
        /// </summary>
        public static string DefaultPrinterName
        {
            get { return getValue("DefaultPrinterName"); }
            set { setValue("DefaultPrinterName", value); }
        }

        /// <summary>
        /// 点検画面での使用プリンタ名
        /// </summary>
        public static string InspectPrinterName
        {
            get { return getValue("InspectPrinterName"); }
            set { setValue("InspectPrinterName", value); }
        }

        /// <summary>
        /// 照会画面での使用プリンタ名
        /// </summary>
        public static string ShokaiPrinterName
        {
            get { return getValue("ShokaiPrinterName"); }
            set { setValue("ShokaiPrinterName", value); }
        }

    
        /// <summary>
        /// データベースのホストアドレス
        /// </summary>
        public static string DataBaseHost
        {
            get {

                //現在使用中のサーバ名からIPを取得                               
                return dsSettings.Tables[CurrentServer].Rows[0]["DBIP"].ToString();
                

            }
            set { setValue("DataBaseHost", value); }
        }

        /// <summary>
        /// データベースのタイムアウト秒
        /// </summary>
        public static string CommandTimeout
        {
            get { return getValue("CommandTimeout"); }
            set { setValue("CommandTimeout", value); }
        }

        /// <summary>
        /// 印刷時マージン
        /// </summary>
        public static int PrintMargin
        {
            get { return int.Parse(getValue("PrintMargin")); }
            set { setValue("PrintMargin", value.ToString()); }
        }

        /// <summary>
        /// 1グループに割り当てられる画像枚数
        /// </summary>
        public static int GroupCount
        {
            get { return int.Parse(getValue("GroupCount")); }
            set { setValue("GroupCount", value.ToString()); }
        }

        /// <summary>
        /// スキャン画像の保存先フォルダ
        /// </summary>
        public static string ImageFolder
        {
            get { return getValue("ImageFolder"); }
            set { setValue("ImageFolder", value); }
        }

        /// <summary>
        /// バージョン管理フォルダ
        /// </summary>
        public static string VersionFolder
        {
            get { return getValue("VersionFolder"); }
            set { setValue("VersionFolder", value); }
        }


        //postgresqlのポート指定もsettingsから取得
        
        /// <summary>
        /// db接続ポート
        /// </summary>
        public static string dbPort
        {
            get { return getValue("port"); }
            set { setValue("port", value); }
        }
        


        //リスト作成画面のベースとなるExcelフォルダパスの取得/設定
        
        public static string BaseExcelDir
        {
            get { return getValue("BaseExcelDir"); }
            set { setValue("BaseExcelDir", value); }
        }
        


        //ユーザ、パスワードをSettings.xmlから取得
        
        public static string JyuseiDBUser
        {
            get { return getValue("jyusei_user"); }
            set { setValue("jyusei_user",value); }
        }

        public static string JyuseiDBPassword
        {
            get { return getValue("jyusei_password"); }
            set { setValue("jyusei_password",value); }
        }
        




        public static string CurrentComboIndex
        {
            get { return getValue("currentComboIndex"); }
            set { setValue("currentComboIndex", value); }
        }



        
        

        /// <summary>
        /// 現在使用中のサーバ名
        /// </summary>
        public static string CurrentServer
        {
            get { return getValue("currentServer"); }
            set { setValue("currentServer", value); }
        }
        
    }
}
