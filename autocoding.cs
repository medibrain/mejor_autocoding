﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace MejorAutoCoding
{

    public partial class autocoding : Form
    {
        DataTable dtFields = new DataTable();
        Npgsql.NpgsqlCommand cmd = new Npgsql.NpgsqlCommand();
        Npgsql.NpgsqlDataAdapter da =new Npgsql.NpgsqlDataAdapter();
        Npgsql.NpgsqlConnection cn = new Npgsql.NpgsqlConnection();
        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        int dgvCurrentRowIndex = 0;

        System.Data.DataTable dtInputFields = new DataTable();

        public autocoding()
        {
            InitializeComponent();
            Settings.getServerSettings();

            disp();
            
        }


        /// <summary>
        /// 表示
        /// </summary>
        private void disp()
        {
            //jyuseiのDBだけはsettings.xmlから取得
            cn.ConnectionString =
                $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=common;User Id={Settings.JyuseiDBUser};" +
                $"Password={Settings.JyuseiDBPassword};SSLMode=Require;CommandTimeout={Settings.CommandTimeout}";
            cn.Open();
            cmd.Connection = cn;

            lblserver.Text = $"Server={Settings.DataBaseHost};Port={Settings.dbPort};Database=common;";

            sb.Remove(0, sb.ToString().Length);
            
            sb.AppendLine(" select ");
            sb.AppendLine(" id,colno, appcol, coltype,appfieldname,inputdatatype, systemfield,typing, inputform, autoset, other, " +
                "flguse, onlyinput, ctlname, valname, maxlengthcheck, maxvaluecheck, dsc1, dsc2, dsc3 ");
            sb.AppendLine(" from");
            sb.AppendLine(" autocoding");

            sb.AppendLine(" order by id");

            cmd.CommandText = sb.ToString();


            da.SelectCommand = cmd;            
            da.Fill(dtFields);
            dgv.DataSource = dtFields;

            dgv.Columns["id"].Width = 30;
            dgv.Columns["colno"].Width = 30;
            dgv.Columns["appcol"].Width = 150;
            dgv.Columns["coltype"].Width = 80;            
            dgv.Columns["appfieldname"].Width = 100;
            dgv.Columns["inputdatatype"].Width = 50;
            dgv.Columns["systemfield"].Width = 50;
            dgv.Columns["typing"].Width = 50;
            dgv.Columns["inputform"].Width = 30;
            dgv.Columns["autoset"].Width = 30;
            dgv.Columns["other"].Width = 30;
            dgv.Columns["flguse"].Width = 30;
            dgv.Columns["onlyinput"].Width = 80;
            dgv.Columns["ctlname"].Width = 150;
            dgv.Columns["valname"].Width = 100;
            dgv.Columns["maxlengthcheck"].Width = 80;
            dgv.Columns["maxvaluecheck"].Width = 80;
            dgv.Columns["dsc1"].Width = 150;
            dgv.Columns["dsc2"].Width = 150;
            dgv.Columns["dsc3"].Width = 150;

            dgv.Columns["id"].HeaderText = "id";
            dgv.Columns["colno"].HeaderText = "列番号";
            dgv.Columns["appcol"].HeaderText = "列名";
            dgv.Columns["coltype"].HeaderText = "データ型";
            dgv.Columns["appfieldname"].HeaderText = "項目名";
            dgv.Columns["inputdatatype"].HeaderText = "入力データ型";
            dgv.Columns["systemfield"].HeaderText = "システム項目";
            dgv.Columns["typing"].HeaderText = "必要項目";
            dgv.Columns["inputform"].HeaderText = "手入力";
            dgv.Columns["autoset"].HeaderText = "自動設定";
            dgv.Columns["other"].HeaderText = "不明";
            dgv.Columns["flguse"].HeaderText = "使用する項目";
            dgv.Columns["onlyinput"].HeaderText = "DBになく入力画面のみ";
            dgv.Columns["ctlname"].HeaderText = "コントロール名";
            dgv.Columns["valname"].HeaderText = "変数名";
            dgv.Columns["maxlengthcheck"].HeaderText = "最大長チェック";
            dgv.Columns["maxvaluecheck"].HeaderText = "最大値チェック";
            dgv.Columns["dsc1"].HeaderText = "メモ1";
            dgv.Columns["dsc2"].HeaderText = "メモ2";
            dgv.Columns["dsc3"].HeaderText = "メモ3";

          
            DataGridViewCellStyle cs = new DataGridViewCellStyle();
            cs.BackColor = Color.LightGray;
            

            dgv.Columns["id"].DefaultCellStyle = cs;
            dgv.Columns["colno"].DefaultCellStyle = cs;
            dgv.Columns["appcol"].DefaultCellStyle = cs;
            dgv.Columns["coltype"].DefaultCellStyle = cs;
            dgv.Columns["appfieldname"].DefaultCellStyle = cs;

            dgv.Columns["inputdatatype"].DefaultCellStyle = cs;
            dgv.Columns["systemfield"].DefaultCellStyle = cs;

            dgv.Columns["typing"].DefaultCellStyle = cs;
            dgv.Columns["inputform"].DefaultCellStyle = cs;
            dgv.Columns["autoset"].DefaultCellStyle = cs;
            dgv.Columns["other"].DefaultCellStyle = cs;

            

            dgv.Columns["id"].ReadOnly =true;
            dgv.Columns["colno"].ReadOnly = true;
            dgv.Columns["appcol"].ReadOnly = true;
            dgv.Columns["coltype"].ReadOnly = true;
            dgv.Columns["appfieldname"].ReadOnly = true;

            dgv.Columns["inputdatatype"].ReadOnly = true;
            dgv.Columns["typing"].ReadOnly = false;//入力すべき項目
            dgv.Columns["inputform"].ReadOnly = false;//手入力する項目
            dgv.Columns["autoset"].ReadOnly = false;//自動設定する項目
            
            dgv.Columns["other"].ReadOnly = true;
            dgv.Columns["other"].Visible = false;//不明


            cmbDup.Items.Clear();
            foreach(DataGridViewColumn c in dgv.Columns)
            {
                cmbDup.Items.Add(c.Name);
            }

            dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex;
            dgv.Columns[2].Frozen = true;//

           // systemfield();
        }

        private void systemfield()
        {
            for(int r =0;r<dgv.RowCount;r++)
            //foreach (DataGridViewRow r in dgv.Rows)
            {
                
                

                if (dgv.Rows[r].Cells["systemfield"].Value.ToString() == "True")
                {
                    

                    dgv.Rows[r].DefaultCellStyle.BackColor = Color.Salmon;
                }
                else
                {
                    

                    dgv.Rows[r].DefaultCellStyle.BackColor=Color.Gray;
                }
            }
        }


        /// <summary>
        /// 登録ボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonOK_Click(object sender, EventArgs e)
        {

            upd();

        }
        


        /// <summary>
        /// 更新
        /// </summary>
        private void upd()
        {
            int col = 0;
            List<string> lstsql = new List<string>();

            foreach (DataRow dr in dtFields.Rows)
            {

                if (dr.RowState == DataRowState.Added)
                {
                    col = 0;

                 
                }
                else if (dr.RowState == DataRowState.Modified)
                {
                    col = 1;

                    sb.Remove(0, sb.ToString().Length);
                    sb.AppendLine(" update autocoding set ");

                    sb.AppendFormat(" colno			    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" appcol		    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" coltype		    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" appfieldname	    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" inputdatatype	    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" systemfield	    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" typing		    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" inputform		    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" autoset		    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" other			    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" flguse		    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" onlyinput		    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" ctlname		    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" valname		    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" maxlengthcheck    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" maxvaluecheck	    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" dsc1			    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" dsc2			    =	'{0}',", dr[col++].ToString());
                    sb.AppendFormat(" dsc3			    =	'{0}' ", dr[col++].ToString());
                    sb.AppendFormat(" where id	    	=   '{0}';", dr[0].ToString());

                    lstsql.Add(sb.ToString());
                }


            }

            if (lstsql.Count == 0) return;

            //cmd.CommandText = sb.ToString();
            Npgsql.NpgsqlTransaction tran;
            tran = cn.BeginTransaction();


            try
            {

                foreach (string s in lstsql)
                {
                    cmd.CommandText = s;
                    cmd.ExecuteNonQuery();
                }
                tran.Commit();
                MessageBox.Show("更新した");
            }
            catch (Exception ex)
            {
                tran.Rollback();
                MessageBox.Show(ex.Message);

            }
            finally
            {
                dtFields.Clear();

                cn.Close();
                disp();
            }


        }

        /// <summary>
        /// id取得
        /// </summary>
        /// <returns></returns>
        private int getMaxID()
        {
            int ret=0;

            sb.Remove(0, sb.ToString().Length);

            sb.AppendLine("select max(insurerid) id from insurer");
            cmd.CommandText = sb.ToString();
            int.TryParse(cmd.ExecuteScalar().ToString(),out ret);

            return ++ret;
            

        }

       

        /// <summary>
        /// 削除ボタン列
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex != 0) return;

            //セルを触ったら現在行を控える
            dgvCurrentRowIndex = dgv.CurrentRow.Index;

            string id = dgv.Rows[dgv.CurrentRow.Index].Cells[1].Value.ToString();
            

        }

        private void SettingForm_Insurer_Shown(object sender, EventArgs e)
        {
            dgv.FirstDisplayedScrollingRowIndex = dgvCurrentRowIndex;
        }

        /// <summary>
        /// 重複チェックボタン
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnChkDup_Click(object sender, EventArgs e)
        {
            string fld = "ｽｷｬﾝｵｰﾀﾞｰ";//デフォルトだけ決めとく
            fld = cmbDup.Text;

            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;

            foreach(DataGridViewRow r in dgv.Rows)
            {
                var tmp = r.Cells[fld].Value.ToString();
                int cnt = 0;

                foreach(DataGridViewRow r2 in dgv.Rows)
                {
                    if (tmp == r2.Cells[fld].Value.ToString()){
                        
                        cnt++;
                        if (cnt > 1)
                        {
                            
                            r.DefaultCellStyle.BackColor = Color.Coral;
                            r2.DefaultCellStyle.BackColor = Color.Coral;
                        }
                        else
                        {
                            r.DefaultCellStyle = null;
                            dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                        }

                    }
                    else
                    {
                        dgv.AlternatingRowsDefaultCellStyle.BackColor = Color.LightGray;
                    }
                  
                }

            }
        }

        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnTextOut_Click(object sender, EventArgs e)
        {
            CreateText();
        }


        /// <summary>
        /// コードのテキストを出力
        /// </summary>
        private void CreateText()
        {
            string strFileName = $"{DateTime.Now.ToString("yyyyMMdd_HHmmss")}_out.txt";
            System.IO.StreamWriter sw = new System.IO.StreamWriter(strFileName);
           
            if (!GetUseControls()) return;
                
            sw.Write(CreateLoadToScreenAppdata());
            sw.Write(CreateLoadToScreenDate());

            sw.Write(CreateCheckInputAppdata());
            sw.Write(CreateCheckDate());

            sw.Write(CreateSetToApp());
         
            sw.Close();
            //テキストを開いておく
            System.Diagnostics.ProcessStartInfo si = new System.Diagnostics.ProcessStartInfo(strFileName);
            System.Diagnostics.Process p = new System.Diagnostics.Process();
            p.StartInfo = si;
            p.Start();
       
            

        }

     

        /// <summary>
        /// 画面にAppを表示するコードの生成
        /// </summary>
        /// <returns></returns>
        private string CreateLoadToScreenAppdata()
        {
            string strret = string.Empty;

            //こんな感じ
            //verifyBoxY.Text = app.MediYear.ToString();
            //verifyBoxM.Text = app.MediMonth.ToString();

            //setValue(verifyBoxY, app.MediYear, firstTime, nv);

            foreach (DataRow r in dtInputFields.Rows)
            {
                string strComment = r["dsc1"].ToString() + (r["dsc2"] == null ? string.Empty : r["dsc2"].ToString());
                string strCtl = r["ctlname"].ToString();
                string strVal = string.Empty;
                int intVal = 0;
                string strCheck = string.Empty;//エラーとする条件
                string strType = r["inputdatatype"].ToString();
                string strAppFieldName = r["appfieldname"].ToString();

                //自動設定の項目は紛らわしいので削除させるコメント
                if (r["autoset"].ToString() == "True") strComment+=" ※コード上は削除対象";

                switch (strType) {
                    case "int":
                        int.TryParse(strVal, out intVal);
                        strret += 
                            $"//{strComment}\r\n" + 
                            $"setValue({strCtl}, app.{strAppFieldName}, firstTime, nv);\r\n\r\n";

                        break;

                    case "string":
                        strret +=
                           $"//{strComment}\r\n" +
                           $"setValue({strCtl}, app.{strAppFieldName}.ToString(), firstTime, nv);\r\n\r\n";



                        break;
                }
            }

            if (strret != string.Empty) strret = "------ SetAppValues to Screen ------------------------\r\n" + strret;
            return strret;
        }


        /// <summary>
        /// 画面にAppを表示するコードの生成　日付型
        /// </summary>
        /// <returns></returns>
        private string CreateLoadToScreenDate()
        {

            // setDateValue(app.FushoFirstDate1, firstTime, nv, verifyBoxF1FirstY, verifyBoxF1FirstM, verifyBoxF1FirstE);

            string strret = string.Empty;
            DataRow[] r = dtInputFields.Select("flguse=true and coltype='date'");
            if (r.Count() == 0) return string.Empty;

            string strappcol = r[0]["appcol"].ToString();
            string strVal = r[0]["appcol"].ToString();
            string strdsc1 = r[0]["dsc1"].ToString();

            strret =
                $"//{strdsc1}\r\n" +
                $"setDateValue(app.{strVal} ,firstTime, nv, ";

            try
            {
                foreach (DataRow dr in r)
                {
                    if (strappcol != dr["appcol"].ToString())
                    {
                        strdsc1 = dr["dsc1"].ToString();
                        strVal = dr["appcol"].ToString();
                        strappcol = dr["appcol"].ToString();

                        strret = strret.Substring(0, strret.Length - 1);
                        strret += ");\r\n\r\n";
                        strret +=
                            $"//{strdsc1}\r\n" +
                            $"setDateValue(app.{strVal}, firstTime, nv, ";
                    }

                    strret += $"{dr["ctlname"].ToString()},";


                }
                strret = strret.Substring(0, strret.Length - 1);
                strret += ");\r\n";



                strret = "--------- setdate ----------------\r\n" + strret;

                return strret;
            }
            catch(Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return string.Empty;
            }
        }



        /// <summary>
        /// 入力チェックのコードの生成 日付型
        /// </summary>
        /// <returns></returns>
        private string CreateCheckDate()
        {

            // DateTime dtDoui = dateCheck(vbDouiG, vbDouiY, vbDouiM, vbDouiD);

            string strret = string.Empty;
            DataRow[] r = dtInputFields.Select("flguse=true and coltype='date'");
            if (r.Count() == 0) return string.Empty;

            string strappcol = r[0]["appcol"].ToString();
            string strVal = r[0]["appcol"].ToString();
            string strdsc1 = r[0]["dsc1"].ToString();

            strret =
                $"//{strdsc1}\r\n" +
                $"DateTime dt{strVal} = dateCheck(";

            try
            {
                foreach (DataRow dr in r)
                {
                    if (strappcol != dr["appcol"].ToString())
                    {
                        strdsc1 = dr["dsc1"].ToString();
                        strVal = dr["appcol"].ToString();
                        strappcol = dr["appcol"].ToString();

                        strret = strret.Substring(0, strret.Length - 1);
                        strret += ");\r\n\r\n";
                        strret +=
                            $"//{strdsc1}\r\n" +
                            $"DateTime dt{strVal} = dateCheck(";
                    }

                    strret += $"{dr["ctlname"].ToString()},";


                }
                strret = strret.Substring(0, strret.Length - 1);
                strret += ");\r\n";



                strret = "--------- date check ----------------\r\n" + strret;

                return strret;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return string.Empty;
            }
        }


        /// <summary>
        /// 入力チェックのコードの生成
        /// </summary>
        /// <returns></returns>
        public string CreateCheckInputAppdata()
        {
            string strret = string.Empty;

            //こんな感じのコード
            //合計金額
            //int total = verifyBoxTotal.GetIntValue();
            //setStatus(verifyBoxTotal, total < 100 | 200000 < total);

            foreach (DataRow r in dtInputFields.Rows)
            {
                string strComment = r["dsc1"].ToString() + (r["dsc2"] == null ? string.Empty : r["dsc2"].ToString());
                string strCtl = r["ctlname"].ToString();
                string strVal = string.Empty;
                string strCheck = string.Empty;//エラーとする条件

                string strInputDataType = r["inputdatatype"].ToString();
                string strMaxLengthCheck = r["maxlengthcheck"].ToString();
                string strMaxValueCheck = r["maxvaluecheck"].ToString();
                bool autoset = bool.Parse(r["autoset"].ToString());

                string strColType = r["coltype"].ToString();//列データ型

                //コントロール名がない場合飛ばす
                if (strCtl == string.Empty) continue;
                //自動設定の項目は飛ばす
                if (autoset) continue;


                if (strColType != "date")
                {

              


                    //文字列制限がある場合、文字列とみなす
                    if (strMaxLengthCheck != string.Empty)
                    {
                        strVal = $"string str{strCtl} = {strCtl}.Text.Trim();";
                        strCheck = $"str{strCtl}.Length > {strMaxLengthCheck}";
                    }
                    //最大値制限がある場合は数値とみなす
                    else if (strMaxValueCheck != string.Empty)
                    {
                        strVal = $"int int{strCtl} = {strCtl}.GetIntValue();";

                        //範囲制限

                        if (strMaxValueCheck.Contains("-"))
                        {

                            strCheck =
                                $"int{strCtl}.Value < {strMaxValueCheck.Split('-')[0]} || " +
                                $"int{strCtl}.Value > {strMaxValueCheck.Split('-')[1]}";
                        }

                        //個々の数字の制限
                        //文字列サンプル：setStatus(verifyBoxJuryoType, !new[] { 0, 2, 4, 6, 8 }.Contains(juryoType));
                        else if (strMaxValueCheck.Contains("|"))
                        {
                            List<string> lst = strMaxValueCheck.Split('|').ToList<string>();

                            strCheck = " !new [] = {";

                            foreach (string s in lst) strCheck += $"{s},";

                            //最後のカンマをとる
                            strCheck = strCheck.Substring(0, strCheck.Length - 1);

                            strCheck += $"}}.Contains(int{strCtl})";

                        }
                    }

                    //何も制限がない
                    else
                    {
                        strVal = $"string str{strCtl} = {strCtl}.Text.Trim();";

                    }

                }



                if (strVal != String.Empty)
                {
                    strret += $"\r\n//{strComment}\r\n" +
                        $"{strVal}" +
                        $"\r\n";

                    if (strCheck != string.Empty)
                    {
                        strret +=
                           $"setStatus({strCtl},{strCheck});" +
                           $"\r\n\r\n";
                    }
                }
            }

            if (strret != string.Empty) strret = "------ checkinput ------------------------\r\n"+strret;
            return strret;
        }


        /// <summary>
        /// チェック後の値をAPPに代入するコードの生成
        /// </summary>
        /// <returns></returns>
        private string CreateSetToApp()
        {
            string strret = string.Empty;


            //こんな感じ
            //app.MediMonth = month;
            //app.HihoNum = verifyBoxHnum.Text;
            //app.Total = total;


            foreach (DataRow r in dtInputFields.Rows)
            {

                string strComment = r["dsc1"].ToString() + (r["dsc2"] == null ? string.Empty : r["dsc2"].ToString());
                string strAppFieldName = r["appfieldname"].ToString();
                string strVal = string.Empty;
                string strCtl = r["ctlname"].ToString();
                string strInputdatatype = r["inputdatatype"].ToString();//列データ型

                switch (strInputdatatype)
                {
                    case "int":
                        strVal = $"int{strCtl}";
                        break;
                    case "string":
                        strVal = $"str{strCtl}";
                        break;
                }

                //自動設定の項目は紛らわしいので削除させるコメント
                if (r["autoset"].ToString() == "True") strComment += " ※コード上は削除対象";


                strret += 
                    $"//{strComment}\r\n" +
                    $"app.{strAppFieldName} = {strVal};\r\n\r\n";


            }


            if (strret != string.Empty) strret = "------ set to app ------------------------\r\n" + strret;

            return strret;
        }


        private bool GetUseControls()
        {
            try
            {
                dtInputFields.Clear();
                cmd.Connection = cn;
                cmd.CommandText = "select * from autocoding where flguse = true or autoset = true order by id";
                da.SelectCommand = cmd;
                da.Fill(dtInputFields);
                return true;
            }
            catch (Exception ex)
            {
                System.Windows.Forms.MessageBox.Show(System.Reflection.MethodBase.GetCurrentMethod().Name + "\r\n" + ex.Message);
                return false;
            }
       

        }

        /// <summary>
        /// 複数行一気にチェック
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgv_KeyDown(object sender, KeyEventArgs e)
        {            
            if (dgv.CurrentCell.ColumnIndex >= 6 && dgv.CurrentCell.ColumnIndex <=11)
            {
                if (e.KeyCode == Keys.Space)
                {
                    foreach(DataGridViewCell c in dgv.SelectedCells)
                    {
                        if (!(bool)c.Value) c.Value = true;
                        else c.Value = false;
                    }

                }
            }
        }
    }
}
